import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import logo from "./logo.svg";
import "./App.css";
import Header from "./layouts/Header";
import NavBar from "./layouts/NavBar";
import Footer from "./layouts/Footer";
import ContentLayout from "./layouts/ContentLayout";
import ExerciseList from "./components/ExerciseList.component";
import EditExercise from "./components/EditExercise.component";
import CreateExercise from "./components/CreateExercise.component";
import CreateUser from "./components/CreateUser.component";

function App() {
  return (
    <Router className="hold-transition sidebar-mini">
        <div className="wrapper">
          <Header />
          <NavBar />
          <Route path="/" exact component={ContentLayout}/>
          <Route path="/list/exercise" component={ExerciseList}/>
          <Route path="/edit/exercise/:id" component={EditExercise}/>
          <Route path="/create/exercise" component={CreateExercise}/>
          <Route path="/create/user" component={CreateUser} />
          <Footer />
        </div>
    </Router>
  );
}

export default App;
