import React, { Component } from "react";
import axios from "axios";

export default class CreateUser extends Component {
  constructor(props){
    super(props);

    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      username: "",
    };
  }

  onChangeUsername(e){
    this.setState({
      username: e.target.value,
    });
  }

  onSubmit(e){
    e.preventDefault();

    const user = {
      username: this.state.username,
    }

    axios.post("http://localhost:5000/users/add", user)
      .then(res => console.log(res.data));

    console.log(user);

    this.setState({
      username: "",
    });
  }

  render() {
    return (
      <div>{/* Content Wrapper. Contains page content */}
      <div className="content-wrapper">
        <section className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1>Create User</h1>
              </div>
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item">
                    <a href="#">Home</a>
                  </li>
                  <li className="breadcrumb-item active">Create User</li>
                </ol>
              </div>
            </div>
          </div>
          {/* /.container-fluid */}
        </section>

        <div className="content">
          <div className="content-fluid">
            <div className="card card-primary">
              <div className="card-header">
                <h3 className="card-title">Create User</h3>
              </div>
              {/* /.card-header */}
              {/* form start */}
              <form onSubmit={this.onSubmit}>
                <div className="card-body">

                  {/* Input username */}
                  <div className="form-group">
                    <label htmlFor="username">Username:</label>
                    <input
                      type="text"
                      id="username"
                      required
                      className="form-control"
                      value={this.state.username}
                      onChange={this.onChangeUsername}
                    />
                  </div>

                  {/* /.card-body */}
                  <div className="card-footer">
                    <input
                      type="submit"
                      value="Create Exercise Log"
                      className="form-control btn-success"
                    />
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      </div>
    );
  }
}
