import React, { Component } from "react";
import axios from "axios";

export default class CreateExercise extends Component {
  constructor(props) {
    super(props);

    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeDuration = this.onChangeDuration.bind(this);
    this.onChangeDate = this.onChangeDate.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      username: "",
      description: "",
      duration: 0,
      date: new Date(),
      users: [],
    };
  }

  componentDidMount() {
    axios.get("http://localhost:5000/users/").then((response) => {
      this.setState({
        users: response.data.map((user) => user.username),
        username: response.data[0].username,
      });
    });
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value,
    });
  }
  onChangeDescription(e) {
    this.setState({
      description: e.target.value,
    });
  }
  onChangeDuration(e) {
    this.setState({
      duration: e.target.value,
    });
  }
  onChangeDate(date) {
    this.setState({
      date: date,
    });
  }

  onSubmit(e) {
    e.preventDefault();

    const exercise = {
      username: this.state.username,
      description: this.state.description,
      duration: this.state.duration,
      date: this.state.date,
    };

    axios
      .post("http://localhost:5000/exercises/add", exercise)
      .then((res) => console.log(res.data));

    console.log(exercise);

    window.location = "/";
  }

  render() {
    return (
      <div>
        <div className="content-wrapper">
          <section className="content-header">
            <div className="container-fluid">
              <div className="row mb-2">
                <div className="col-sm-6">
                  <h1>Create Exercise</h1>
                </div>
                <div className="col-sm-6">
                  <ol className="breadcrumb float-sm-right">
                    <li className="breadcrumb-item">
                      <a href="#">Home</a>
                    </li>
                    <li className="breadcrumb-item active">Create Exercise</li>
                  </ol>
                </div>
              </div>
            </div>
          </section>

          <div className="content">
            <div className="content-fluid">
              <div className="card card-primary">
                <div className="card-header">
                  <h3 className="card-title">Create Exercise</h3>
                </div>
                <form onSubmit={this.onSubmit}>
                  <div className="card-body">
                    <div className="form-group">
                      <label ref="userInput">Username</label>
                      <select
                        id="selectUser"
                        ref="userInput"
                        required
                        className="form-control"
                        value={this.state.username}
                        onChange={this.onChangeUsername}
                      >
                        {this.state.users.map(function (user) {
                          return (
                            <option key={user} value={user}>
                              {user}
                            </option>
                          );
                        })}
                      </select>
                    </div>

                    <div className="form-group">
                      <label>Description:</label>
                      <input
                        type="text"
                        id="description"
                        required
                        className="form-control"
                        value={this.state.description}
                        onChange={this.onChangeDescription}
                      />
                    </div>

                    <div className="form-group">
                      <label>Duration:</label>
                      <input
                        id="duration"
                        type="text"
                        className="form-control"
                        value={this.state.duration}
                        onChange={this.onChangeDuration}
                      />
                    </div>

                    <div className="form-group">
                      <label>Date: </label>
                      <div>
                        <input type="date" className="form-control" id="date" />
                      </div>
                    </div>

                    <div className="card-footer">
                      <input
                        type="submit"
                        value="Create Exercise Log"
                        className="form-control btn-success"
                      />
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
